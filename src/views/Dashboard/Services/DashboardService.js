import axios from 'axios';

const GET_USER_AVAILABILITIES = "http://localhost:9004/medx/api/dashboard/availabilites";

const GET_CITY_SERVICE_REQUESTS = "http://localhost:9004/medx/api/dashboard/cityServices";

class DashboardService {
    getUsers() {
        return axios.get(GET_USER_AVAILABILITIES);
    }

    getCities() {
        return axios.get(GET_CITY_SERVICE_REQUESTS);
    }
}

export default new DashboardService();