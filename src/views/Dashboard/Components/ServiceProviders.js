import React, { Component } from "react";
import { Card } from "react-bootstrap";
import './StyleSheets/ServiceProvider.css'
function ServiceProviders(props) {
  return (
    <div className="col-lg-4 col-md-6 col-sm-8 col-xs-12">
      
     <div className="card" style={{padding:20}}>
     <div className="card-body">
        
        <div className="stat-heading" style={{textAlign:'left',marginBottom:20,fontSize:30}}>Service Providers</div>
        
        <br />
            <hr />
            <br />
            
            <div className="item1">
                <div className="item-title" style={{float:"left"}}>
                Active
                </div>
                <i
                className={`fa fa-${props.icon1} ${props.color1} fa-3x`}  style={{float:"right"}}
                aria-hidden="true"
              >
              {props.count1}
              </i>
            </div>
            <br />
            <br />
            <hr />
            <div className="item2">
                <div className="item-title" style={{float:"left"}}>
                Inactive
                </div>
                <i
                className={`fa fa-${props.icon2} ${props.color2} fa-3x`}  style={{float:"right"}}
                aria-hidden="true"
              >
              {props.count2}
              </i>
            </div>
            <br />
            <br />
            <hr />
            
            <div className="item2">
                <div className="item-title" style={{float:"left"}}>
               At Work
                </div>
                <i
                className={`fa fa-${props.icon3} ${props.color3} fa-3x`}  style={{float:"right"}}
                aria-hidden="true"
              >
              {props.count3}
              </i>
            </div>
            
         
        </div>
      </div>
    </div>
  );
}

export default ServiceProviders;