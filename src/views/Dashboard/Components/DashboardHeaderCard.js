
import React, { Component } from "react";

function DashboardHeaderCard(props) {
  return (
    <div className="col-lg-4 mb-4 col-sm-6 col-md-6 col-xs-12">
     <div className="card" style={{padding:20}}>
        <div className="card-body">
        
        <div className="stat-heading" style={{paddingBottom:80,textAlign:'left',marginBottom:20,fontSize:30}}>{props.title}</div>
        
        <div className="text-left" style={{float:'left'}}>
        <div className="stat-text">
            <span className="count">
            <i
                className={`fa fa-${props.icon1} ${props.color1} fa-4x`}
                aria-hidden="true"
              >
              {props.count1}
              </i></span>
          </div>


          
        <div className="stat-heading" style={{textAlign:'center',padding:20}}>{props.info1}</div>
          
         </div>
         

         
        <div className="text-right"  style={{float:'right'}}>
        <div className="stat-text">
        <span className="count">
            <i
                className={`fa fa-${props.icon2} ${props.color2} fa-4x`}
                aria-hidden="true"
              >
              {props.count2}
              </i></span>
            </div>
        <div className="stat-heading" style={{textAlign:'center',padding:20}}>{props.info2}</div>
        
         </div>
         
        </div>
      </div>
    </div>
  );
}

export default DashboardHeaderCard;
